class User {
  String avatar = "";
  String groups = "";
  int id = 0;
  String introduction = "";
  String mst = "";
  String name = '';
  String pwd = '';
  String roles = "";

  User({this.avatar = "", this.groups = "", this.id = 0, this.introduction = "", this.mst = "", required this.name, required this.pwd, this.roles = ""});

  User.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'] ? json['avatar'] : "";
    groups = json['groups'] ? json['groups'] : "";
    id = json['id'] ? json['id'] : 0;
    introduction = json['introduction'] ? json['introduction'] : "";
    mst = json['mst'] ? json['mst'] : "";
    name = json['name'] ? json['name'] : '';
    pwd = json['pwd'] ? json['pwd'] : '';
    roles = json['roles'] ? json['roles'] : "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['groups'] = this.groups;
    data['id'] = this.id;
    data['introduction'] = this.introduction;
    data['mst'] = this.mst;
    data['name'] = this.name;
    data['pwd'] = this.pwd;
    data['roles'] = this.roles;
    return data;
  }
}
