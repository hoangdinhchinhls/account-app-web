import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:accoutant_app/login/login.dart';
import 'package:formz/formz.dart';
import 'package:wc_form_validators/wc_form_validators.dart';
import 'package:accoutant_app/login/view/Animation/FadeAnimation.dart';
import 'package:accoutant_app/login/models/user.dart';
import 'package:accoutant_app/services/user_api.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String _userName = '';
  String _password = '';

  final _formKey = GlobalKey<FormState>();
  // const LoginForm({Key? key}) : super(key: key);
  void _trySubmitForm() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      try {
        final user = User(name: _userName, pwd: _password);
        var userInfo = await UserProvider().login(user);
        print('user info' + userInfo.toJson().toString());
      } catch (_) {
        print('login exception' + _.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Container(
            height: 400,
            decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/background.png'), fit: BoxFit.fill)),
            child: Stack(
              children: <Widget>[
                Positioned(
                  left: 30,
                  width: 80,
                  height: 200,
                  child: FadeAnimation(
                      1,
                      Container(
                        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/light-1.png'))),
                      )),
                ),
                Positioned(
                  left: 140,
                  width: 80,
                  height: 150,
                  child: FadeAnimation(
                      1.3,
                      Container(
                        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/light-2.png'))),
                      )),
                ),
                Positioned(
                  right: 40,
                  top: 40,
                  width: 80,
                  height: 150,
                  child: FadeAnimation(
                      1.5,
                      Container(
                        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/clock.png'))),
                      )),
                ),
                Positioned(
                  child: FadeAnimation(
                      1.6,
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Center(
                          child: Text(
                            "Login",
                            style: TextStyle(color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold),
                          ),
                        ),
                      )),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(30.0),
            child: Column(
              children: <Widget>[
                FadeAnimation(
                    1.8,
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10), boxShadow: [
                        BoxShadow(color: Color.fromRGBO(143, 148, 251, .2), blurRadius: 20.0, offset: Offset(0, 10))
                      ]),
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(8.0),
                            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey))),
                            child: TextField(
                              onChanged: (value) => _userName = value,
                              decoration: InputDecoration(border: InputBorder.none, hintText: "Tên đăng nhập", hintStyle: TextStyle(color: Colors.grey[400])),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8.0),
                            child: TextFormField(
                              obscureText: true,
                              onChanged: (value) => _password = value,
                              validator: Validators.minLength(6, 'mật khẩu phải có ít nhất 6 kí tự'),
                              decoration: InputDecoration(border: InputBorder.none, hintText: "Mật khẩu", hintStyle: TextStyle(color: Colors.grey[400])),
                            ),
                          )
                        ],
                      ),
                    )),
                SizedBox(height: 30),
                FadeAnimation(
                    2,
                    GestureDetector(
                        onTap: _trySubmitForm,
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              gradient: LinearGradient(colors: [
                                Color.fromRGBO(143, 148, 251, 1),
                                Color.fromRGBO(143, 148, 251, .6),
                              ])),
                          child: Center(
                            child: Text(
                              "Đăng nhập",
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ),
                        )))
                // SizedBox(
                //   height: 70,
                // ),
                // FadeAnimation(
                //     1.5,
                //     Text(
                //       "Forgot Password?",
                //       style: TextStyle(color: Color.fromRGBO(143, 148, 251, 1)),
                //     )),
              ],
            ),
          )
        ],
      ),
    );
  }
}

// class _UsernameInput extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<LoginBloc, LoginState>(
//       buildWhen: (previous, current) => previous.username != current.username,
//       builder: (context, state) {
//         return Container(
//           padding: EdgeInsets.all(8.0),
//           decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey))),
//           child: TextField(
//             onChanged: (username) => context.read<LoginBloc>().add(LoginUsernameChanged(username)),
//             decoration: InputDecoration(
//               border: InputBorder.none,
//               hintText: "Tên đăng nhập",
//               hintStyle: TextStyle(color: Colors.grey[400]),
//               errorText: state.username.invalid ? 'invalid username' : null,
//             ),
//           ),
//         );
//       },
//     );
//   }
// }

// class _PasswordInput extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<LoginBloc, LoginState>(
//       buildWhen: (previous, current) => previous.password != current.password,
//       builder: (context, state) {
//         return Container(
//           padding: EdgeInsets.all(8.0),
//           child: TextFormField(
//             obscureText: true,
//             onChanged: (password) => context.read<LoginBloc>().add(LoginPasswordChanged(password)),
//             validator: Validators.minLength(6, 'mật khẩu phải có ít nhất 6 kí tự'),
//             decoration: InputDecoration(
//               border: InputBorder.none,
//               hintText: "Mật khẩu",
//               hintStyle: TextStyle(color: Colors.grey[400]),
//               errorText: state.password.invalid ? 'invalid password' : null,
//             ),
//           ),
//         );
//       },
//     );
//   }
// }

// class _LoginButton extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<LoginBloc, LoginState>(
// //      buildWhen: (previous, current) => previous.status != current.status,
//       builder: (context, state) {
//         return FadeAnimation(
//           2,
//           GestureDetector(
//               onTap: () {
//                 _trySubmitForm
//                 // context.read<LoginBloc>().add(const LoginSubmitted());
//               },
//               child: Container(
//                 height: 50,
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(10),
//                     gradient: LinearGradient(colors: [
//                       Color.fromRGBO(143, 148, 251, 1),
//                       Color.fromRGBO(143, 148, 251, .6),
//                     ])),
//                 child: Center(
//                   child: Text(
//                     "Đăng nhập",
//                     style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
//                   ),
//                 ),
//               )
//             ),
//         );
//       },
//     );
//   }
// }
