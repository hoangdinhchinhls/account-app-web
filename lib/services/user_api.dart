import 'dart:convert';

import 'package:accoutant_app/globals.dart';
import 'package:accoutant_app/login/models/user.dart';
import 'package:http/http.dart' as http;

class UserProvider {
  Future<User> login(User user) async {
    var client = http.Client();
    var url = Uri.parse(GlobalVariable.API_URL + '/users/login');
    print('url $url');
    print('url: ' + url.toString());
    print('body post:' + jsonEncode(user));

    final response = await client.post(url,
        headers: {
          "Content-Type": "application/json;charset=UTF-8"
        },
        body: jsonEncode(user));
    print('response ' + response.statusCode.toString());
    // print('response body: ' + response.body.toString());
    if (response.statusCode == 200) {
      String data = response.body;
      final jsonObject = jsonDecode(data);
      return User.fromJson(jsonObject);
    } else {
      throw Exception('user name and password maybe incorrect');
    }
  }
}
